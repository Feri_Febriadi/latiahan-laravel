<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class FormController extends Controller
{
    public function bio(){
        return view('halaman.form');
    } 

    public function submit(Request $request){ 
        $nama = $request['nama'];
        $alamat = $request['address'];
        return view('halaman.home', compact('nama', 'alamat'));
    } 
}
